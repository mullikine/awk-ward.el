;;; awk-ward.el --- Run and edit Awk programs -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Maintainer: Kisaragi Hiu
;; Version: 0.3.0
;; Package-Requires: ((emacs "25"))
;; Homepage: https://gitlab.com/kisaragi-hiu/awk-ward.el
;; Keywords: editing convenience


;; This file is not part of GNU Emacs

;; didyoumean.el is free software: you can redistribute it and/or modify
;; it under the terms of the MIT License.

;;; Commentary:

;; Try out Awk scripts right in your editor.

;;; Code:

(defgroup awk-ward nil
  "Awk-ward."
  :group 'editing
  :prefix "awk-ward-")

(defcustom awk-ward-command "awk"
  "Name or full path to the Awk executable."
  :group 'awk-ward
  :type 'path)

(defcustom awk-ward-options nil
  "Options for Awk command."
  :group 'awk-ward
  :type '(repeat string))

(defconst awk-ward-temp-files
  `((program . ,(concat temporary-file-directory "awkwardel-program"))
    (input . ,(concat temporary-file-directory "awkwardel-input")))
  "Alist of Awk-ward's temporary files.")

(defvar awk-ward--window-configuration nil
  "Saved window configuration before entering Awk-ward.")

(defvar awk-ward-program-buffer "*Awk-ward Program*"
  "Name of buffer containing the Awk program.")
(defvar awk-ward-output-buffer "*Awk-ward Output*"
  "Name of buffer for the Awk output.")
(defvar awk-ward-input-buffer "*Awk-ward Input*"
  "Name of buffer for the Awk input.")
(defvar awk-ward--buffers-alist `((program . ,awk-ward-program-buffer)
                                  (input . ,awk-ward-input-buffer)
                                  (output . ,awk-ward-output-buffer))
  "Alist of Awk-ward buffers.")

(defun awk-ward-show ()
  "Show Awk-ward buffers."
  ;; don't save window configuration if we're already in a Awk-ward buffer
  (unless (rassoc (buffer-name (current-buffer)) awk-ward--buffers-alist)
    (setq awk-ward--window-configuration (current-window-configuration)))
  (let-alist awk-ward--buffers-alist
    (delete-other-windows)
    (split-window-below)
    (split-window-below)
    (set-window-buffer (selected-window) (get-buffer-create .input))
    (windmove-down)
    (set-window-buffer (selected-window) (get-buffer-create .output))
    (windmove-down)
    (set-window-buffer (selected-window) (get-buffer-create .program))))

;;;###autoload
(defun awk-ward-start (program-buffer input-buffer cmd opts)
  "Start Awk-ward looking at PROGRAM-BUFFER and INPUT-BUFFER.

Use current buffer as the program buffer if it's in Awk mode.
Otherwise, use `awk-ward-program-buffer'.

With a \\[universal-argument], prompt for a file to visit as the
INPUT-BUFFER."
  (interactive
   (list (or (and (eq major-mode 'awk-mode)
                  (current-buffer))
             awk-ward-program-buffer)
         (or (and current-prefix-arg
                  (find-file-noselect (read-file-name "Input file: " nil nil t)))
             awk-ward-input-buffer)
         awk-ward-command
         awk-ward-options))
  (awk-ward--setup `((program . ,program-buffer)
                     (input . ,input-buffer)
                     (output . ,awk-ward-output-buffer))
                   cmd
                   opts
                   )
  (awk-ward-show))

;;;###autoload
(defalias 'awk-ward #'awk-ward-start)

(defun awk-ward--setup (buffers-alist cmd opts)
  "Set up Awk-ward visiting buffers in BUFFERS-ALIST.

BUFFERS-ALIST looks like:
\((program . PROGRAM-BUFFER)
  (input . INPUT-BUFFER)
  (output . OUTPUT-BUFFER)"
  (setq awk-ward--buffers-alist buffers-alist)
  (let-alist buffers-alist
    (with-current-buffer (get-buffer-create .program)
      (make-local-variable 'awk-ward-command)
      (make-local-variable 'awk-ward-options)
      (setq awk-ward-command cmd)
      (setq awk-ward-options opts)
      (awk-mode)
      (awk-ward-mode))
    (with-current-buffer (get-buffer-create .input)
      (make-local-variable 'awk-ward-command)
      (make-local-variable 'awk-ward-options)
      (setq awk-ward-command cmd)
      (setq awk-ward-options opts)
      (text-mode)
      (awk-ward-mode)
      ;; (tv (concat cmd " " (buffer-name)))
      )
    (with-current-buffer (get-buffer-create .output)
      (make-local-variable 'awk-ward-command)
      (make-local-variable 'awk-ward-options)
      (setq awk-ward-command cmd)
      (setq awk-ward-options opts)
      (awk-ward-mode))))

(defun awk-ward-stop ()
  "Stop Awk-ward.

Also remove Awk-ward buffers and temporary files, and restore
window configuration."
  (interactive)
  (pcase-dolist (`(,_ . ,buf) awk-ward--buffers-alist)
    (when (get-buffer buf)
      (with-current-buffer buf
        (awk-ward-mode -1)
        (kill-current-buffer))))
  (mapc #'delete-file (mapcar #'cdr awk-ward-temp-files))
  (when awk-ward--window-configuration
    (set-window-configuration awk-ward--window-configuration))
  (setq awk-ward--window-configuration nil)
  (message "Awk-ward stopped"))

(define-minor-mode awk-ward-mode
  "Minor mode for Awk-ward buffers."
  :group 'awk-ward :global nil :init-value nil
  (if awk-ward-mode
      (progn
        (add-hook 'post-command-hook #'awk-ward--update nil t)
        (when (featurep 'evil)
          (add-hook 'evil-insert-state-exit-hook #'awk-ward--update nil t))
        (add-hook 'kill-buffer-hook #'awk-ward-stop nil t))
    (remove-hook 'post-command-hook #'awk-ward--update t)
    (remove-hook 'kill-buffer-hook #'awk-ward-stop t)
    (when (featurep 'evil)
      (remove-hook 'evil-insert-state-exit-hook #'awk-ward--update t))))

(defvar awk-ward-mode-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "i") #'awk-ward-change-input)
    (define-key map (kbd "p") #'awk-ward-change-program)
    (define-key map (kbd "q") #'awk-ward-stop)
    map)
  "Non-prefixed keymap for Awk-ward buffers.")

(defvar awk-ward-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c $") awk-ward-mode-command-map)
    map)
  "Keymap for Awk-ward buffers.")

(defun awk-ward-change-input (buf)
  "Make BUF the input buffer.

Interactively, select a file to visit."
  (interactive `(,(find-file-noselect (read-file-name "Input file: " nil nil t))))
  (with-current-buffer (alist-get 'input awk-ward--buffers-alist)
    (awk-ward-mode -1))
  (with-current-buffer buf
    (awk-ward-mode 1))
  (setf (alist-get 'input awk-ward--buffers-alist) buf)
  (awk-ward-show))

(defun awk-ward-change-program (buf)
  "Make BUF the input buffer.

Interactively, select a file to visit."
  (interactive `(,(find-file-noselect (read-file-name "Awk program: " nil nil t))))
  (with-current-buffer (alist-get 'program awk-ward--buffers-alist)
    (awk-ward-mode -1))
  (with-current-buffer buf
    (awk-ward-mode 1))
  (setf (alist-get 'program awk-ward--buffers-alist) buf)
  (awk-ward-show))

(defun awk-ward--update (&optional program-buffer input-buffer output-buffer)
  "Update Awk-ward output.

Take Awk program from PROGRAM-BUFFER, input from INPUT-BUFFER,
and write output to OUTPUT-BUFFER.

Use `awk-ward--buffers-alist' as the fallback if arguments are nil."
  (let-alist awk-ward--buffers-alist
    (unless program-buffer (setq program-buffer .program))
    (unless input-buffer (setq input-buffer .input))
    (unless output-buffer (setq output-buffer .output)))
  (let ((input-file (alist-get 'input awk-ward-temp-files))
        (program-file (alist-get 'program awk-ward-temp-files)))
    ;; From `write-region':
    ;; If VISIT is neither t nor nil nor a string, do not display the
    ;; "Wrote file" message.
    ;; Wat?
    (with-current-buffer input-buffer
      (write-region (point-min) (point-max) input-file nil :dont))
    (with-current-buffer program-buffer
      (write-region (point-min) (point-max) program-file nil :dont))
    (awk-ward--run program-file input-file output-buffer)))

(defun awk-ward--run (program input output-buf)
  "Run Awk PROGRAM with INPUT, writing output to OUTPUT-BUF.

Both PROGRAM and INPUT are paths to files."
  (with-current-buffer (get-buffer-create output-buf)
    (delete-region (point-min) (point-max)))
  (make-process
   :name "awk-ward"
   :buffer output-buf
   :command `(,awk-ward-command ,@awk-ward-options "-f" ,program ,input)
   :sentinel #'ignore
   :filter (lambda (process string)
             (with-current-buffer (process-buffer process)
               (insert string)
               (goto-char (point-min))
               (set-marker (process-mark process) (point))))))

(provide 'awk-ward)

;;; awk-ward.el ends here
