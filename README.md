# Awk-ward.el - Run Awk programs in Emacs

Emacs port of [Awk-ward](https://gitlab.com/HiPhish/awk-ward.nvim).

> Awk-ward allows you to try out Awk scripts right in your editor; you provide the input as either a file or a buffer and the output is displayed in an output buffer. Best of all, you don't even have to write your program or data to disc, Awk-ward will read straight from the buffer, and you can experiment without breaking any of your programs.

## Usage

Run `awk-ward`. Three buffers will be presented; put your raw data in `*Awk-ward Input*`, write your Awk program in `*Awk-ward Program*`, and the result will be shown in `*Awk-ward Output*`.

To use an existing file as the raw data, run <kbd>C-u M-x</kbd> `awk-ward`. You will be prompted to open the input file.

## Project Status

Awk-ward.el is usable, but there may be some bugs.

## License

MIT, see `LICENSE`.
